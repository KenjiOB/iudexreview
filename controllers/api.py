import tempfile

# from gluon.utils import web2py_uuid

def get_forum_posts():
    forum_posts = []
    rows = db().select(db.forum_post.ALL)
    for r in rows:
        p = dict(
            id = r.id,
            post_content = r.post_content,
            created_on = r.created_on,
            user_id = r.user_id,
            user_first_name = r.user_first_name,
            user_last_name = r.user_last_name
        )
        forum_posts.append(p)
    logged_in = auth.user is not None
    current_user = auth.user_id if logged_in else -1
    return response.json(dict(
        forum_posts=forum_posts,
        logged_in=logged_in,
        current_user=current_user
    ))

def get_movies():
    movie_list = []
    rows = db().select(db.movies.ALL)
    for r in rows:
        p = dict(
            id = r.id,
            name = r.name,
        )
        movie_list.append(p)
    logged_in = auth.user is not None
    current_user = auth.user_id if logged_in else -1
    return response.json(dict(
        movie_list=movie_list,
    ))

def get_shows():
    show_list = []
    rows = db().select(db.shows.ALL)
    for r in rows:
        p = dict(
            id = r.id,
            name = r.name,
        )
        show_list.append(p)
    logged_in = auth.user is not None
    current_user = auth.user_id if logged_in else -1
    return response.json(dict(
        show_list=show_list,
    ))

def get_people():
    people_list = []
    rows = db().select(db.people.ALL)
    for r in rows:
        p = dict(
            id = r.id,
            name = r.name,
        )
        people_list.append(p)
    logged_in = auth.user is not None
    current_user = auth.user_id if logged_in else -1
    return response.json(dict(
        people_list=people_list,
    ))

@auth.requires_signature()
def add_forum_post():
    db.forum_post.insert(
        post_content=request.vars.post_content
    )
    p = db().select(db.forum_post.ALL)
    return response.json(dict(forum_post=p))

@auth.requires_signature()
def delete_forum_post():
    db(db.forum_post.id == request.vars.id).delete()
    return dict()

def search_database():   
    fake_movies = ["movie1", "movie2", "movie3", "movie4", "movie5"]
    fake_shows = ["show1", "show2", "show3", "show4", "show5"]
    fake_people = ["person1", "person2", "person3", "person4", "person5"]

    movies = []
    shows = []
    people = []

    if request.vars.search_content is not None:
        searches = request.vars.search_content.split("+")
    else:
        return dict()

    # print searches

    for s in searches:

#------------This is the code for how it would work in the SQL database------------#
        # q = ((db.movies.name.contains(s)) |
        #      (db.shows.name.contains(s))  |
        #      (db.people.name.contains(s)))
        #
        # movies = db(q).select(db.movies.ALL)
        # shows = db(q).select(db.shows.ALL)
        # people = db(q).select(db.people.ALL)

        for movie in fake_movies:
            if s in movie:
                movies.append(movie)

        for show in fake_shows:
            if s in show:
                shows.append(show)

        for person in fake_people:
            if s in person:
                people.append(person)

    return response.json(dict(
        movies=movies,
        shows=shows,
        people=people
    ))

def add_movie():
    db.movies.insert(
        name=request.vars.name,
    )

    t = db().select(db.movies.ALL)
    return response.json(dict(movies=t))


def del_movie():
    db(db.movies.name == request.vars.movie_name).delete()
    return dict()

def add_show():
    db.shows.insert(
        name=request.vars.name,
    )

    t = db().select(db.shows.ALL)
    return response.json(dict(shows=t))


def del_show():
    db(db.shows.name == request.vars.show_name).delete()
    return dict()

def add_people():
    db.people.insert(
        name=request.vars.name,
    )

    t = db().select(db.people.ALL)
    return response.json(dict(people=t))


def del_people():
    db(db.people.name == request.vars.people_name).delete()
    return dict()
