// This is the js for the default/index.html view.

var app = function() {

    var self = {};

    Vue.config.silent = false; // show all warnings

    // Extends an array
    self.extend = function(a, b) {
        for (var i = 0; i < b.length; i++) {
            a.push(b[i]);
        }
    };

    var enumerate = function(v) { var k=0; return v.map(function(e) {e._idx = k++;});};

    var slideIndex = 0;

    // Next/previous controls
    self.plus_slides = function plus_slides(n) {
      self.show_slides(slideIndex += n);
      console.log("Hit")
    };

    // Thumbnail image controls
    self.current_slide = function current_slide(n) {
      self.show_slides(slideIndex = n);
    };

    self.show_slides = function show_slides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slideIndex++;
        if (slideIndex > slides.length) {slideIndex = 1}
        if(self.vue.logged_in === false){
            slides[slideIndex-1].style.display = "block";
        }
        setTimeout(self.show_slides, 3000); // Change image every 3 seconds
    };

    self.go_to_media = function (media) {
        let filter = 'media';
        if (media.type === 'person')
            filter = 'actors';
        window.location.href = filter + "?id=" + media.id + "&type=" + media.type;
    };

    self.setup = function () {
        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                document.getElementById("homepage").style.display = "none";
                document.getElementById("my_content").style.display = "block";
                self.vue.logged_in = true;
                let myLikes = firebase.database().ref('users/' + user.uid + '/favourites');
                myLikes.on('value', function (likesSnapshot) {
                    likesSnapshot.forEach(function(likeSnapshot) {
                        let obj = {};
                        obj.id = likeSnapshot.val()['id'];
                        obj.type = likeSnapshot.val()['type'];
                        obj.title = likeSnapshot.val()['title'];
                        obj.poster = likeSnapshot.val()['poster'];
                        console.log(obj);
                        if (obj.type === 'movie')
                            self.vue.movies.push(obj);
                        else if (obj.type === 'tv')
                            self.vue.shows.push(obj);
                        else
                            self.vue.people.push(obj);
                    });
                });
            } else {
                document.getElementById("my_content").style.display = "none";
                document.getElementById("homepage").style.display = "block";
                self.vue.logged_in = false;
                self.show_slides(slideIndex);
            }
        });
    };

    // Complete as needed.
    self.vue = new Vue({
        el: "#vue-div",
        delimiters: ['${', '}'],
        unsafeDelimiters: ['!{', '}'],
        data: {
            star_indices: [1, 2, 3, 4, 5],
            logged_in: false,
            movies: [],
            shows: [],
            people: []
        },
        methods: {
            current_slide: self.current_slide,
            plus_slides: self.plus_slides,
            setup: self.setup,
            go_to_media: self.go_to_media
        }
    });

    self.setup();
    $("#vue-div").show();

    return self;
};

var APP = null;

// This will make everything accessible from the js console;
// for instance, self.x above would be accessible as APP.x
jQuery(function(){APP = app();});
