const functions = require('firebase-functions');
const request = require('request');
const cors = require('cors')({origin: true});
const imdb = require('imdb-api');
const imdb_api_key = '653bf834';

exports.getSearchResultsByTitle = functions.https.onRequest((req, res) => {
   cors(req, res, () => {
        if (req.query.title !== undefined || req.query.type !== undefined) {
            let url = 'https://api.themoviedb.org/3/search/' + req.query.type +
                '?api_key=d593c53e640a33dda5d7035a97650f6b&language=en-US&query=' +
                req.query.title + '&page=1&include_adult=false';
            console.log("URL ======" + url);
            // make the request
            request(url, function (error, result, body) {
                if (!error && result.statusCode === 200) {
                    res.status(200).send(body);
                }
            });
        } else {
            res.send("Empty search text");
        }
   })
});

exports.getResultById = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        if (req.query.id !== undefined || req.query.type !== undefined) {
            let url = 'https://api.themoviedb.org/3/' + req.query.type + '/' + req.query.id +
                '?api_key=d593c53e640a33dda5d7035a97650f6b&language=en-US';
            // make the request
            request(url, function (error, result, body) {
                if (!error && result.statusCode === 200) {
                    res.status(200).send(body);
                }
            });
        } else {
            res.send("Empty id field");
        }
    });
});

exports.getActorByName = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        if (req.query.name !== undefined) {
            let url = 'https://api.themoviedb.org/3/search/person' +
                '?api_key=d593c53e640a33dda5d7035a97650f6b&language=en-US&query=' +
                req.query.name + '&page=1&include_adult=false';
            // make the request
            request(url, function (error, result, body) {
                if (!error && result.statusCode === 200) {
                    res.status(200).send(body);
                }
            });
        } else {
            res.send("Empty search name");
        }
    });
});


exports.getActorById = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        if (req.query.id !== undefined) {
            let url = 'https://api.themoviedb.org/3/person/' + req.query.id +
                '?api_key=d593c53e640a33dda5d7035a97650f6b&language=en-US';
            // make the request
            request(url, function (error, result, body) {
                if (!error && result.statusCode === 200) {
                    res.status(200).send(body);
                }
            });
        } else {
            res.send("Empty search id");
        }
    });
});

exports.getTrailerById = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        if (req.query.id !== undefined || req.query.type !== undefined) {
            let url = 'http://api.themoviedb.org/3/' + req.query.type + '/' + req.query.id + '/videos' +
                '?api_key=d593c53e640a33dda5d7035a97650f6b';
            // make the request
            request(url, function (error, result, body) {
                if (!error && result.statusCode === 200) {
                    res.status(200).send(body);
                }
            });
        } else {
            res.send("Empty search text");
        }
    });
});

exports.getPopularByType = functions.https.onRequest((req, res) => {
    cors(req, res, () => {
        if (req.query.type !== undefined) {
            let url = 'https://api.themoviedb.org/3/' + req.query.type +
                '/popular?api_key=d593c53e640a33dda5d7035a97650f6b&language=en-US&page=1';
            // make the request
            request(url, function (error, result, body) {
                if (!error && result.statusCode === 200) {
                    res.status(200).send(body);
                }
            });
        } else {
            res.send("Empty type text");
        }
    });
});