// This is the js for the search_results.html view.

var app = function() {

    var self = {};

    Vue.config.silent = false; // show all warnings

    self.go_to_result = function (id) {
        let filter = 'media';
        if (self.vue.type === 'person')
            filter = 'actors';
        window.location.href = filter + "?id=" + id + "&type=" + self.vue.type;
    };

    function httpGetAsync(url, callback) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
                callback(xmlHttp.responseText);
            }
        };
        xmlHttp.open("GET", url, true); // true for asynchronous
        xmlHttp.send(null);
    }

    self.set_search_results = function (response) {
        let results = JSON.parse(response)['results'];
        let title_text = '';
        let picture_text = '';
        if (self.vue.search_filter === 0) {
            title_text = 'title';
            picture_text = 'poster_path';
        } else if (self.vue.search_filter === 1) {
            title_text = 'name';
            picture_text = 'poster_path';
        } else {
            title_text = 'name';
            picture_text = 'profile_path';
        }
        for (var i = 0; i < results.length; i++) {
            if (self.vue.results.length >= 60)
                break;
            let new_result = {};
            new_result.title = results[i][title_text];
            if (results[i][picture_text] != null) {
                new_result.poster = 'http://image.tmdb.org/t/p/w500' + results[i][picture_text];
            } else {
                new_result.poster = 'https://imgur.com/KJXiE9m.jpg';
            }
            new_result.id = results[i]['id'];
            self.vue.results.push(new_result);
        }
    };

    self.get_search_results = function () {
        httpGetAsync(self.vue.functions_url + 'getSearchResultsByTitle?title=' + self.vue.stored_search_content + '&type=' +
            self.vue.type, self.set_search_results);
    };

    self.search_redirect = function() {
        if (self.vue.search_content != null && self.vue.search_content.length > 0) {
            c = self.vue.search_content.trim().replace(/ /g,"+");
            if (c.length > 0) {
                localStorage.search_content = c;
                window.location.href = "search_results?q=" + c;
            }
        }
    };

    self.get_url_params = function () {
    	if ((location.search.split("f=")[1]||"").split("&")[0] === "movies") {
    		self.vue.search_filter = 0;
    		self.vue.type = 'movie';
    	} else if ((location.search.split("f=")[1]||"").split("&")[0] === "shows") {
    		self.vue.search_filter = 1;
    		self.vue.type = 'tv';
    	} else if ((location.search.split("f=")[1]||"").split("&")[0] === "people") {
    		self.vue.search_filter = 2;
    		self.vue.type = 'person';
    	}
    	self.vue.stored_search_content = localStorage.search_content.split("+").join(" ");
        self.get_search_results();
    };

    // Complete as needed.
    self.vue = new Vue({
        el: "#vue-div",
        delimiters: ['${', '}'],
        unsafeDelimiters: ['!{', '}'],
        data: {
            search_content: null,
            results: [],
            search_filter: null,
            type: '',
            stored_search_content: null,
            functions_url: 'https://us-central1-cmps-183-d6150.cloudfunctions.net/'
        },
        methods: {
            search_redirect: self.search_redirect,
            set_search_results: self.set_search_results,
            go_to_result: self.go_to_result
        }

    });

    $("#vue-div").show();
    self.get_url_params();
    return self;
};

var APP = null;

// This will make everything accessible from the js console;
// for instance, self.x above would be accessible as APP.x
jQuery(function(){APP = app();});