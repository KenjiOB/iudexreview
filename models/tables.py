# Define your tables below (or better in another model file) for example
#
# >>> db.define_table('mytable', Field('myfield', 'string'))
#
# Fields can be 'string','text','password','integer','double','boolean'
#       'date','time','datetime','blob','upload', 'reference TABLENAME'
# There is an implicit 'id integer autoincrement' field
# Consult manual for more options, validators, etc.

db.define_table('forum_post', 
                Field('post_content', 'string'),
                Field('created_on', 'datetime', default=request.now),
                Field('user_id', default=auth.user_id if auth.user else -1),
                Field('user_first_name', default=auth.user.first_name if auth.user else 'Anonymous'),
                Field('user_last_name', default=auth.user.last_name if auth.user else '')
                )

db.define_table('people',
             Field('name'),
             Field('user_email', default=auth.user.email if auth.user else None),
)

db.define_table('movies',
             Field('name'),
             Field('user_email', default=auth.user.email if auth.user else None),
)

db.define_table('shows',
             Field('name'),
             Field('user_email', default=auth.user.email if auth.user else None),
)

# after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)
