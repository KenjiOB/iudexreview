// This is the js for the search_results.html view.

var app = function() {

    var self = {};

    Vue.config.silent = false; // show all warnings

    self.sign_up = function () {
        self.vue.showError = false;
        if (self.vue.signing_up) {

            if (self.vue.email === '' || self.vue.username === '' || self.vue.password === ''
                    || self.vue.passwordConfirm === '') {
                    self.vue.showError = true;
                    self.vue.errorMsg = 'Please fill in all the fields';
            } else if (self.vue.password !== self.vue.passwordConfirm) {
                self.vue.showError = true;
                self.vue.errorMsg = 'Passwords don\'t match';
            } else {
                let users = firebase.database().ref('users/').orderByChild('username').equalTo(self.vue.username);
                let found = false;
                users.on('value', function (snapshot) {
                    snapshot.forEach(function(snapshot) {
                        found = true;
                    });
                    if (found) {
                        self.vue.showError = true;
                        self.vue.errorMsg = 'Username already exists';
                    } else {
                        firebase.auth().createUserWithEmailAndPassword(self.vue.email, self.vue.password)
                            .then(function (user) {
                                firebase.database().ref('users/' + user.uid).set({
                                    'username': self.vue.username
                                });
                                self.vue.logged_in = true;
                                window.location.href="../";
                            })
                            .catch(function(error) {
                                self.vue.showError = true;
                                self.vue.errorMsg = error.message;
                            });
                    }
                });
            }
        } else
            self.vue.signing_up = true;
    };

    self.login = function () {
        self.vue.showError = false;
        if (self.vue.signing_up) {
            self.vue.signing_up = false;
        }
        else {
            firebase.auth().signInWithEmailAndPassword(self.vue.email, self.vue.password)
                .then(function () {
                    self.vue.logged_in = true;
                    window.location.href="../";
                })
                .catch(e=>{
                self.vue.showError = true;
                self.vue.errorMsg = e.message;
            })
        }
    };

    self.login_with_google = function () {
        self.vue.showError = false;
        firebase.auth().signInWithRedirect(self.vue.googleProvider).then(function () {
        });
        firebase.auth().getRedirectResult().then(function(result) {
          if (result.credential) {
            // This gives you a Google Access Token. You can use it to access the Google API.
            var token = result.credential.accessToken;
          }
          // The signed-in user info.
          var user = result.user;
        }).catch(function(error) {
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          // The email of the user's account used.
          var email = error.email;
          // The firebase.auth.AuthCredential type that was used.
          var credential = error.credential;
          // ...
        });
    };

    self.setup = function () {
        var config = {
            apiKey: "AIzaSyDO8eWxXstUgVCSf54yftzRxdcmpyewhYo",
            authDomain: "cmps-183-d6150.firebaseapp.com",
            databaseURL: "https://cmps-183-d6150.firebaseio.com",
            projectId: "cmps-183-d6150",
            storageBucket: "cmps-183-d6150.appspot.com",
            messagingSenderId: "1028450050563"
        };
        firebase.initializeApp(config);

        self.vue.auth = firebase.auth();
        self.vue.googleProvider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().onAuthStateChanged(user=>{
            if (user) {
                if (self.vue.username === '')
                    self.vue.username = user.displayName;
                self.vue.logged_in = true;
                firebase.database().ref('users/' + user.uid).set({
                    'username': self.vue.username
                }).then(() => {
                    console.log(self.vue.username);
                    window.location.href="../";
                });
            }
        });
    };

    // Complete as needed.
    self.vue = new Vue({
        el: "#vue-div",
        delimiters: ['${', '}'],
        unsafeDelimiters: ['!{', '}'],
        data: {
            auth: null,
            googleProvider: null,
            logged_in: false,
            signing_up: false,
            email: '',
            username: '',
            password: '',
            passwordConfirm: '',
            showError: false,
            errorMsg: '',
        },
        methods: {
            setup: self.setup,
            login_with_google: self.login_with_google,
            login: self.login,
            sign_up: self.sign_up
        }

    });

    self.setup();
    $("#vue-div").show();
    return self;
};

var APP = null;

// This will make everything accessible from the js console;
// for instance, self.x above would be accessible as APP.x
jQuery(function(){APP = app();});