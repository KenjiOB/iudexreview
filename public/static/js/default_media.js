// This is the js for the default/movie.html view.

var app = function() {

    var self = {};

    Vue.config.silent = false; // show all warnings

    // Extends an array
    self.extend = function(a, b) {
        for (var i = 0; i < b.length; i++) {
            a.push(b[i]);
        }
    };

    var enumerate = function(v) { var k=0; return v.map(function(e) {e._idx = k++;});};

    self.mouse_over = function (star_idx) {
        self.vue.num_stars_display = star_idx;
    };

    self.mouse_out = function() {
        self.vue.num_stars_display = self.vue.num_stars;
    };

    self.set_stars = function(star_idx) {
        if (firebase.auth().currentUser) {
            self.vue.num_stars = star_idx;
            let old_num_of_ratings = self.vue.num_of_ratings;
            if (!self.vue.rated_by_user) {
                self.vue.num_of_ratings += 1;
                self.vue.rated_by_user = true;
            }
            let new_rating = ((self.vue.our_rating * old_num_of_ratings) - self.vue.old_rating + star_idx) / self.vue.num_of_ratings;
            firebase.database().ref(self.vue.type + '/' + self.vue.id + '/rating').set({
                value: new_rating,
                num_of_ratings: self.vue.num_of_ratings
            });
            self.vue.old_rating = star_idx;
            self.vue.our_rating = new_rating;
            firebase.database().ref('users/' + firebase.auth().currentUser.uid + '/ratings/' + self.vue.id).set({
                value: star_idx
            });
        }
    };

    self.toggle_like_button = function () {
        if (firebase.auth().currentUser) {
            self.vue.is_liked = !self.vue.is_liked;
            if (self.vue.is_liked) {
                firebase.database().ref('users/' + firebase.auth().currentUser.uid + '/favourites').push({
                    'id': self.vue.id,
                    'type': self.vue.type,
                    'title': self.vue.title,
                    'poster': self.vue.poster
                });
            } else {
                let myLikes = firebase.database().ref('users/' + firebase.auth().currentUser.uid + '/favourites').orderByChild('id').equalTo(self.vue.id);
                myLikes.on('value', function (likesSnapshot) {
                    likesSnapshot.forEach(function(likeSnapshot) {
                        if (!self.vue.is_liked) {
                            likeSnapshot.ref.remove();
                        }
                    });
                });
            }
        }
    };

    self.get_forum_posts = function () {
        let posts = firebase.database().ref(self.vue.type + '/' + self.vue.id + '/posts');
        posts.on('value', function (snapshot) {
            snapshot.forEach(function(item) {
                var found = self.vue.forum_posts.some(function (post) {
                    return post.key === item.key;
                });
                if (!found) {
                    let itemVal = item.val();
                    let show_del = false;
                    if (firebase.auth().currentUser.uid === itemVal.user_id)
                        show_del = true;
                    itemVal.show_delete = show_del;
                    itemVal.key = item.key;
                    self.vue.forum_posts.push(itemVal);
                }
            });
            self.vue.forum_posts.reverse();
        });
    };

    self.add_forum_post = function () {
        if (!self.illegal_post() && firebase.auth().currentUser) {
            firebase.database().ref(self.vue.type + '/' + self.vue.id + '/posts').push({
                post_content: self.vue.new_post_content,
                user_id: firebase.auth().currentUser.uid,
                created_on: new Date().toDateString()
            });
        }
    };

    self.illegal_post = function () {
        if (self.vue.new_post_content == null || self.vue.new_post_content.replace(/\s/g, '').length < 1) {
            alert("Your post cannot be empty.");
            return true;
        } else {
            for ( var i = 0; i < self.vue.forum_posts.length; i++) {
                if (self.vue.new_post_content === self.vue.forum_posts[i].post_content) {
                    if (self.vue.current_user === self.vue.forum_posts[i].user_id) {
                        alert("Duplicate post already exists.");
                        return true;
                    }
                }
            }
        }
        return false;
    };

    self.delete_forum_post = function (post_id) {
        let posts = firebase.database().ref(self.vue.type + '/' + self.vue.id + '/posts');
        posts.on('value', function (snapshot) {
            snapshot.forEach(function(item) {
                if (item.key === post_id) {
                    item.ref.remove();
                var idx = null;
                for (var i = 0; i < self.vue.forum_posts.length; i++) {
                    if (self.vue.forum_posts[i].key === post_id) {
                        idx = i + 1;
                        break;
                    }
                }
                if (idx) {
                    self.vue.forum_posts.splice(idx - 1, 1);
                }
                }
            });
        });
    };

    self.setLiked = function (type, id) {
        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                let myLikes = firebase.database().ref('users/' + user.uid + '/favourites').orderByChild('id').equalTo(id);
                myLikes.on('value', function (likesSnapshot) {
                    likesSnapshot.forEach(function(likeSnapshot) {
                        self.vue.is_liked = true;
                    });
                });
            }
        });
    };

    self.set_trailer = function (response) {
        let obj = JSON.parse(response);
        let results = obj['results'];
        if(results[0] !== undefined)
            self.vue.trailer = 'http://www.youtube.com/embed/' + results[0].key;
    };

    self.change_date = function (time) {
        var r = time.match(/^\s*([0-9]+)\s*-\s*([0-9]+)\s*-\s*([0-9]+)(.*)$/);
        return r[2]+"-"+r[3]+"-"+r[1];
    };

    self.initialise = function (response) {
        let obj = JSON.parse(response);
        self.vue.title = obj['title'];
        if (obj['title'] != null) {
            self.vue.title = obj['title'];
        } else {
            self.vue.title = obj['name'];
        }
        if(obj['release_date'] !== undefined){
            self.vue.year = "Release date: "+self.change_date(obj['release_date']);
        }
        if(obj['runtime'] !== undefined){
            self.vue.runtime = "Runtime: "+obj['runtime'] +" Minutes";
        }
        self.vue.genre = "Genre: "+obj['genres'][0].name;
        self.vue.plot = obj['overview'];
        self.vue.rating = obj['vote_average'];
        if (obj['poster_path'] != null)
            self.vue.poster = 'http://image.tmdb.org/t/p/w500' + obj['poster_path'];
        else
            self.vue.poster = 'https://imgur.com/KJXiE9m.jpg';

        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                self.vue.logged_in = true;
                let myLikes = firebase.database().ref('users/' + user.uid + '/favourites').orderByChild('id').equalTo(self.vue.id);
                myLikes.on('value', function (likesSnapshot) {
                    likesSnapshot.forEach(function(likeSnapshot) {
                        self.vue.is_liked = true;
                    });
                });

                let myRating = firebase.database().ref('users/' + user.uid + '/ratings/' + self.vue.id);
                myRating.on('value', function (snapshot) {
                    snapshot.forEach(function(ratingSnapshot) {
                        self.vue.rated_by_user = true;
                        self.vue.old_rating = ratingSnapshot.val();
                    });
                });
            }

            let rating = firebase.database().ref(self.vue.type + '/' + self.vue.id + '/rating');
            rating.on('value', function (snap) {
                if (snap.val() != null) {
                    self.vue.num_of_ratings = snap.val()['num_of_ratings'];
                    self.vue.our_rating = snap.val()['value'];
                    self.vue.num_stars = Math.round(self.vue.our_rating);
                    self.vue.num_stars_display = self.vue.num_stars;
                }
            });
        });

        self.get_forum_posts();
    };

    function httpGetAsync(url, callback) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
                callback(xmlHttp.responseText);
            }
        };
        xmlHttp.open("GET", url, true); // true for asynchronous
        xmlHttp.send(null);
    }

    self.strip_get = function(movie) {
        var string = movie.trim();
        var n_string = string.slice(0, string.indexOf(" "));
        return n_string;
    };

    self.setup = function () {
        let url_string = window.location.href;
        let url = new URL(url_string);
        self.vue.id = url.searchParams.get("id");
        self.vue.type = url.searchParams.get("type");
        httpGetAsync(self.vue.functions_url + 'getResultById?id=' + self.vue.id + '&type=' + self.vue.type, self.initialise);
        httpGetAsync(self.vue.functions_url + 'getTrailerById?id=' + self.vue.id + '&type=' + self.vue.type, self.set_trailer);
    };

    // Complete as needed.
    self.vue = new Vue({
        el: "#vue-div",
        delimiters: ['${', '}'],
        unsafeDelimiters: ['!{', '}'],
        data: {
            id: 0,
            type: null,
            title: '',
            year: '',
            runtime: '',
            genre: '',
            plot: '',
            rating: '',
            our_rating: 0,
            num_of_ratings: 0,
            rated_by_user: false,
            old_rating: 0,
            poster: 'https://imgur.com/79HZjma.jpg',
            trailer: '',
            is_liked: false,
            num_stars_display: 0,
            num_stars: 0,
            star_indices: [1, 2, 3, 4, 5],
            forum_posts: [],
            logged_in: false,
            current_user: -1,
            new_post_content: null,
            movie_results: [],
            show_results: [],
            people_results: [],
            database: firebase.database(),
            functions_url: 'https://us-central1-cmps-183-d6150.cloudfunctions.net/'
        },
        methods: {
            toggle_like_button: self.toggle_like_button,
            mouse_over: self.mouse_over,
            mouse_out: self.mouse_out,
            set_stars: self.set_stars,
            add_forum_post: self.add_forum_post,
            delete_forum_post: self.delete_forum_post,
            setLiked: self.setLiked,
            setup: self.setup,
            initialise: self.initialise,
            set_trailer: self.set_trailer
        }

    });

    self.setup();
    // $("#movie-vue-div").show();

    $("#vue-div").show();
    return self;
};

var APP = null;

// This will make everything accessible from the js console;
// for instance, self.x above would be accessible as APP.x
jQuery(function(){APP = app();});