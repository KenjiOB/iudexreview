var config = {
    apiKey: "AIzaSyDO8eWxXstUgVCSf54yftzRxdcmpyewhYo",
    authDomain: "cmps-183-d6150.firebaseapp.com",
    databaseURL: "https://cmps-183-d6150.firebaseio.com",
    projectId: "cmps-183-d6150",
    storageBucket: "cmps-183-d6150.appspot.com",
    messagingSenderId: "1028450050563"
};

firebase.initializeApp(config);
const auth = firebase.auth();
// const promise = auth.signInWithEmailAndPassword(email, pass);
// auth.createUserWithEmailAndPassword(email, pass);
const googleProvider = new firebase.auth.GoogleAuthProvider();
// const googlePromise = auth.signInWithPopup(googleProvider);

document.getElementById("btnSignUp").addEventListener('click', e=> {
    document.getElementById("txtUsername").style.visibility = 'visible';
    document.getElementById("txtPasswordConfirm").style.visibility = 'visible';
    const email = document.getElementById("txtEmail").value;
    const pass = document.getElementById("txtPassword").value;
    // firebase.auth().createUserWithEmailAndPassword(email, pass).catch(function(error) {
    //  console.log(error.message);
    // });
});

document.getElementById("btnLogin").addEventListener('click', e=>{
  const email = document.getElementById("txtEmail").value;
  const pass = document.getElementById("txtPassword").value;
  const promise = firebase.auth().signInWithEmailAndPassword(email, pass);
  promise.catch(e=>{ document.getElementById("errormsg").style.display = "block"})

});

document.getElementById("withGoogle").addEventListener('click', e=>{
    firebase.auth().signInWithRedirect(googleProvider);
    firebase.auth().getRedirectResult().then(function(result) {
      if (result.credential) {
        // This gives you a Google Access Token. You can use it to access the Google API.
        var token = result.credential.accessToken;
        // document.getElementById("login_button").value = "Log out";
        // window.location.href = '../';
      }
      // The signed-in user info.
      var user = result.user;
    }).catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // The email of the user's account used.
      var email = error.email;
      // The firebase.auth.AuthCredential type that was used.
      var credential = error.credential;
      // ...
    });
});

// document.getElementById("btnLogOut").addEventListener('click', e=>{
//   firebase.auth().signOut();
//   console.log('logged out')
// });

firebase.auth().onAuthStateChanged(user=>{
  if(user) {
      document.getElementById("login_button").value = "Log out";
      window.location.href="../";
      // setTimeout(function(){location.href="../", 200} );
  } else {
      document.getElementById("login_button").value = "Login";
  }
});