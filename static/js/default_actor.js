// This is the js for the default/movie.html view.

var app = function() {

    var self = {};

    Vue.config.silent = false; // show all warnings

    // Extends an array
    self.extend = function(a, b) {
        for (var i = 0; i < b.length; i++) {
            a.push(b[i]);
        }
    };

    var enumerate = function(v) { var k=0; return v.map(function(e) {e._idx = k++;});};

    self.change_date = function (time) {
        var r = time.match(/^\s*([0-9]+)\s*-\s*([0-9]+)\s*-\s*([0-9]+)(.*)$/);
        return r[2]+"-"+r[3]+"-"+r[1];
    };

    self.go_to_movie = function (id, type) {
        window.location.href = "media?id=" + id + "&type=" + type;
    };

    self.toggle_like_button = function () {
        if (firebase.auth().currentUser) {
            self.vue.is_liked = !self.vue.is_liked;
            if (self.vue.is_liked) {
                firebase.database().ref('users/' + firebase.auth().currentUser.uid + '/favourites').push({
                    'id': self.vue.id,
                    'type': self.vue.type,
                    'title': self.vue.name,
                    'poster': self.vue.poster
                });
            } else {
                let myLikes = firebase.database().ref('users/' + firebase.auth().currentUser.uid + '/favourites').orderByChild('id').equalTo(self.vue.id);
                myLikes.on('value', function (likesSnapshot) {
                    likesSnapshot.forEach(function(likeSnapshot) {
                        if (!self.vue.is_liked) {
                            likeSnapshot.ref.remove();
                        }
                    });
                });
            }
        }
    };

    self.set_known_for = function(response) {
        let obj = JSON.parse(response);
        let results = obj['results'];
        let movies = [];
        for (let i = 0; i < results.length; i++) {
            if (parseInt(results[i]["id"], 10) === parseInt(self.vue.id, 10)) {
                movies = results[i]['known_for'];
            }
        }
        for (let i = 0; i < movies.length; i++) {
            if (self.vue.known_for.length >= 5)
                break;
            let movie = {};
            if (movies[i]['title'] != null) {
                movie.title = movies[i]['title'];
            } else {
                movie.title = movies[i]['name'];
            }
            if (movies[i]['poster_path'] != null) {
                movie.poster = 'http://image.tmdb.org/t/p/w500' + movies[i]['poster_path'];
            } else {
                movie.poster = 'https://imgur.com/KJXiE9m.jpg';
            }
            movie.id = movies[i]['id'];
            movie.type = movies[i]['media_type'];
            self.vue.known_for.push(movie);
        }
    };

    self.initialise = function (response) {
        let obj = JSON.parse(response);
        self.vue.name = obj['name'];
        console.log(obj['birthday'])
        if(obj['birthday'] != undefined || obj['birthday'] != null) {
            self.vue.birthday = self.change_date(obj['birthday']);
        }
        else{
            self.vue.birthday = obj['birthday'];
        }
        if(obj['deathday'] != undefined || obj['deathday'] != null) {
            self.vue.deathday = self.change_date(obj['deathday']);
        }
        else{
            self.vue.deathday = obj['deathday'];
        }
        self.vue.biography = obj['biography'];
        self.vue.poster = 'http://image.tmdb.org/t/p/w500' + obj['profile_path'];
        if (obj['gender'] === 1)
            self.vue.gender = 'Female';
        else
            self.vue.gender = 'Male';
        self.vue.place_of_birth = obj['place_of_birth'];

        httpGetAsync(self.vue.functions_url + 'getActorByName?name=' + self.vue.name, self.set_known_for);

        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                self.vue.logged_in = true;
                let myLikes = firebase.database().ref('users/' + user.uid + '/favourites').orderByChild('id').equalTo(self.vue.id);
                myLikes.on('value', function (likesSnapshot) {
                    likesSnapshot.forEach(function(likeSnapshot) {
                        self.vue.is_liked = true;
                    });
                });
            }
        });
    };

    function httpGetAsync(url, callback) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
                callback(xmlHttp.responseText);
            }
        };
        xmlHttp.open("GET", url, true); // true for asynchronous
        xmlHttp.send(null);
    }

    self.setup = function () {
        let url_string = window.location.href;
        let url = new URL(url_string);
        self.vue.id = url.searchParams.get("id");
        httpGetAsync(self.vue.functions_url + 'getActorById?id=' + self.vue.id, self.initialise);
    };

    // Complete as needed.
    self.vue = new Vue({
        el: "#vue-div",
        delimiters: ['${', '}'],
        unsafeDelimiters: ['!{', '}'],
        data: {
            id: 0,
            type: 'person',
            is_liked: false,
            logged_in: false,
            name: '',
            birthday: '',
            deathday: '',
            biography: '',
            poster: 'https://imgur.com/79HZjma.jpg',
            gender: '',
            place_of_birth: '',
            known_for: [],
            database: firebase.database(),
            functions_url: 'https://us-central1-cmps-183-d6150.cloudfunctions.net/'
        },
        methods: {
            toggle_like_button: self.toggle_like_button,
            setup: self.setup,
            initialise: self.initialise,
            set_known_for: self.set_known_for,
            go_to_movie: self.go_to_movie
        }

    });

    self.setup();
    // $("#movie-vue-div").show();

    $("#vue-div").show();
    return self;
};

var APP = null;

// This will make everything accessible from the js console;
// for instance, self.x above would be accessible as APP.x
jQuery(function(){APP = app();});