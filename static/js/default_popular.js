// This is the js for the search_results.html view.

var app = function() {

    var self = {};

    Vue.config.silent = false; // show all warnings

    self.go_to_result = function (id) {
        let filter = 'media';
        if (self.vue.type === 'person')
            filter = 'actors';
        window.location.href = filter + "?id=" + id + "&type=" + self.vue.type;
    };

    function httpGetAsync(url, callback) {
        let xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() {
            if (xmlHttp.readyState === 4 && xmlHttp.status === 200) {
                callback(xmlHttp.responseText);
            }
        };
        xmlHttp.open("GET", url, true); // true for asynchronous
        xmlHttp.send(null);
    }

    self.set_results = function (response) {
        let results = JSON.parse(response)['results'];
        let title_text = '';
        let picture_text = '';
        if (self.vue.type === 'movie') {
            title_text = 'title';
            picture_text = 'poster_path';
        } else if (self.vue.type === 'tv') {
            title_text = 'name';
            picture_text = 'poster_path';
        } else {
            title_text = 'name';
            picture_text = 'profile_path';
        }
        for (var i = 0; i < results.length; i++) {
            console.log(results[i]);
            if (self.vue.results.length >= 40)
                break;
            let new_result = {};
            new_result.title = results[i][title_text];
            if (results[i][picture_text] != null) {
                new_result.poster = 'http://image.tmdb.org/t/p/w500' + results[i][picture_text];
            } else {
                new_result.poster = 'https://imgur.com/KJXiE9m.jpg';
            }
            new_result.id = results[i]['id'];
            self.vue.results.push(new_result);
        }
    };

    self.get_results = function () {
        httpGetAsync(self.vue.functions_url + 'getPopularByType?type=' + self.vue.type, self.set_results);
    };

    self.get_url_params = function () {
    	if ((location.search.split("type=")[1]||"").split("&")[0] === "movies") {
    		self.vue.type = 'movie';
    		self.vue.type_text = 'movies';
    	} else if ((location.search.split("type=")[1]||"").split("&")[0] === "shows") {
    		self.vue.type = 'tv';
    		self.vue.type_text = 'TV shows';
    	} else if ((location.search.split("type=")[1]||"").split("&")[0] === "people") {
    		self.vue.type = 'person';
    		self.vue.type_text = 'people';
    	}
    	self.get_results();
    };

    // Complete as needed.
    self.vue = new Vue({
        el: "#vue-div",
        delimiters: ['${', '}'],
        unsafeDelimiters: ['!{', '}'],
        data: {
            results: [],
            type: '',
            type_text: '',
            functions_url: 'https://us-central1-cmps-183-d6150.cloudfunctions.net/'
        },
        methods: {
            get_results: self.get_results,
            set_results: self.set_results,
            go_to_result: self.go_to_result
        }

    });

    $("#vue-div").show();
    self.get_url_params();
    return self;
};

var APP = null;

// This will make everything accessible from the js console;
// for instance, self.x above would be accessible as APP.x
jQuery(function(){APP = app();});